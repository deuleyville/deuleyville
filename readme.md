## Hello world!

This wine’s dusty perfume carries the fragrance of red cherries and roses. Pintas Character comes from a 1970 planting, a field blend of 30 varieties, the parcel adjacent to the 1930 planting of more than 35 varieties that produces Pintas. It’s seamless, with the sort of balance that should guarantee a long life. Fresh, firm, stony and brisk, hinting at apple skin and lemon zest, this will reward patient cellaring.

Starts out round and juicy, boasting cherry, plum, earth and iron flavors. This Riserva hits all the right notes, the dark and juicy fruit flavors tingling with dried herbs and subtle spices. This is, by far, the best wine from Willm we’ve ever tasted, a brisk, bright, mood-changing wine. He founded his 22.2-acre domaine in 2005, based on old-vine parcels, and converted them to organic farming. Today, the vines at this site date to 1962 and 1976 and are farmed with organic practices by Kevin Fontaine.


